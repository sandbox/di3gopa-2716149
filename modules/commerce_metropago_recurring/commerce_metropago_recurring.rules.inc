<?php

/**
 * @file
 * Provides Rules integration
 */

/**
 * Implements hook_rules_data_info().
 */
function commerce_metropago_recurring_rules_data_info() {
  return array(
    'commerce_metropago_recurring_find_reference_response' => array(
      'label' => t('reference id response'),
      'group' => t('Metropago Recurring'),
      'wrap' => TRUE,
      'wrapper class' => 'EntityStructureWrapper',
      'token type' => 'commerce-metropago-order-find-reference',
      'property info' => commerce_metropago_recurring_order_find_reference_property_info_callback(),
    ),
  );
}

/**
 * Implements hook_rules_event_info().
 */
function commerce_metropago_recurring_rules_event_info() {
  $events = array();
  $events['commerce_metropago_recurring_charge_failed'] = array(
    'label' => t('After a failed attempt to charge an order'),
    'group' => t('Metropago Recurring'),
    'access callback' => 'commerce_order_rules_access',
    'variables' => array(
      'card_data' => array(
        'type' => 'commerce_metropago_recurring',
        'label' => t('Card'),
        'optional' => TRUE,
      ),
      'order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
      ),
      'charge' => array(
        'type' => 'commerce_price',
        'label' => t('Charge'),
        'optional' => TRUE,
      ),
      'response' => array(
        'type' => 'commerce_metropago_recurring_find_reference_response',
        'label' => t('charge Card Response'),
      ),
    ),
  );
  $events['commerce_metropago_recurring_charge_success'] = array(
    'label' => t('After a successful chargeing of an order'),
    'group' => t('Metropago Recurring'),
    'access callback' => 'commerce_order_rules_access',
    'variables' => array(
      'card_data' => array(
        'type' => 'commerce_metropago_recurring',
        'label' => t('Card'),
      ),
      'order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
      ),
      'charge' => array(
        'type' => 'commerce_price',
        'label' => t('Charge'),
      ),
      'response' => array(
        'type' => 'commerce_metropago_recurring_find_reference_response',
        'label' => t('charge Card Response'),
      ),
    ),
  );

  return $events;
}

/**
 * Implements hook_rules_action_info().
 */
function commerce_metropago_recurring_rules_action_info() {
  $actions = array();
  $actions['commerce_metropago_recurring_order_find_reference'] = array(
    'label' => t('Find a reference id'),
    'group' => t('Metropago Recurring'),
    'parameter' => array(
      'order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
      ),
    ),
    'provides' => array(
      'find_reference_response' => array(
        'type' => 'commerce_metropago_recurring_find_reference_response',
        'label' => t('Find Reference response'),
        'save' => FALSE,
      ),
    ),
    'callbacks' => array(
      'execute' => 'commerce_metropago_recurring_rules_action_order_find_reference',
    ),
  );

  $actions['commerce_metropago_recurring_order_charge_card'] = array(
    'label' => t('charge an order with a card on file'),
    'group' => t('Metropago Recurring'),
    'parameter' => array(
      'order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
      ),
      'charge' => array(
        'type' => 'commerce_price',
        'label' => t('Charge'),
        'description' => t('The charge amount and currency. If not provided, then the order balance will be used.'),
        'optional' => TRUE,
      ),
      'find_reference_response' => array(
        'type' => 'commerce_metropago_recurring_find_reference_response',
        'label' => t('Select Card Response'),
        'description' => t('If provided, Card Data parameter can be omitted.'),
        'optional' => TRUE,
      ),
    ),
    'provides' => array(
      'charge_card_response' => array(
        'type' => 'commerce_metropago_recurring_find_reference_response',
        'label' => t('Charge Card Response'),
        'save' => FALSE,
      ),
    ),
    'callbacks' => array(
      'execute' => 'commerce_metropago_recurring_rules_action_order_charge_card',
    ),
  );


  return $actions;
}

/**
 * Options list for payment method instances that provide a charge callback
 */
function commerce_metropago_recurring_rules_payment_instance_charge_options_list() {
  $options = array('' => t('- None -'));
  $implements = commerce_metropago_recurring_payment_method_implements('charge callback');

  foreach ($implements as $method_id => $method_function) {
    $payment_method_instances = _commerce_metropago_recurring_payment_method_instances($method_id, TRUE);
    if (empty($payment_method_instances)) {
      continue;
    }

    foreach ($payment_method_instances as $instance_id => $payment_method) {
      list($method_id_part, $rule_name) = explode('|', $instance_id);

      $options[$instance_id] = t('@title (rule: @rule)', array(
        '@title' => $payment_method['title'],
        '@rule' => $rule_name,
      ));
    }
  }

  return $options;
}

/**
 * Rules action callback for commerce_metropago_recurring_order_select_card
 */
function commerce_metropago_recurring_rules_action_order_find_reference($order) {

  $response = commerce_metropago_recurring_order_find_reference($order);

  return array('find_reference_response' => $response);
}

/**
 * Rules action callback for commerce_metropago_recurring_order_charge_card.
 */
function commerce_metropago_recurring_rules_action_order_charge_card($order, $charge = NULL, $find_reference_response = NULL) {
  // This action uses the found reference on previous rule to process
  // A new payment.
  if (empty($find_reference_response) || !$find_reference_response['status']) {
    $response = array(
      'status' => FALSE,
      'message' => 'No ballot provided to charge card.',
    );
    rules_invoke_all('commerce_metropago_recurring_charge_failed', NULL, $order, $charge, $response);
    return array('charge_card_response' => $response);
  }

  // Pass the data to payment preprocess function.
  $response = commerce_metropago_recurring_order_charge_card($order, $charge, $find_reference_response);
  return array('charge_card_response' => $response);
}
