<?php

/**
 * @file
 * Default rule configuration for Commerce Card on File recurring.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_metropago_recurring_default_rules_configuration() {
  $rules = array();
  // Find and charge recurring orders.
  $rule = rules_reaction_rule();
  $rule->label = 'Charge pending recurring orders';
  $rule->requires = array(
    'rules',
    'commerce_recurring',
  );
  $rule->event('commerce_recurring_cron');
  $rule->action('entity_query', array(
    'type' => 'commerce_order',
    'property' => 'status',
    'value' => 'recurring_pending',
    'limit' => 5,
  ));
  $loop = rules_loop(array(
    'list:select' => 'entity-fetched',
    'item:var' => 'order',
    'item:label' => t('Current Order'),
  ));
  $loop->action('commerce_metropago_recurring_order_find_reference', array(
    'order:select' => 'order',
  ));
  $loop->action('commerce_metropago_recurring_order_charge_card', array(
    'order:select' => 'order',
    'charge:select' => 'order:commerce-order-total',
    'find_reference_response:select' => 'find-reference-response',
  ));
  $rule->action($loop);
  $rules['charge_pending_recurring_orders'] = $rule;

  return $rules;
}

